package pickapp.landmarker.mapas;

import com.google.android.gms.maps.GoogleMap;

import java.util.List;

import pickapp.landmarker.bean.Checkpoint;
import pickapp.landmarker.bean.Travel;

public interface MapsView {
    void requestLocationPermissions();
    void showTravelMap(Checkpoint checkpoint);
    void showListOfTravels();
    void startTravel(String travelId);
    void startLMLocationListener();
    void setLocationEnabled(GoogleMap googleMap);
    void removeGPSUpdates();
    void setToolbarTitle(String title);
    void setCheckpoints(List<Checkpoint> checkpoints);
    void showChallengeDialog();
    void showError(String error);
}
