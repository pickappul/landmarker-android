package pickapp.landmarker.mapas;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import pickapp.landmarker.bean.Checkpoint;

class MapReadyCallback implements OnMapReadyCallback {
    GoogleMap mMap;
    Checkpoint initialCheckpoint;
    MapsView mapsView;
    UiSettings uiSettings;
    LatLng userPosition;
    IMapsPresenter mapsPresenter;


    MapReadyCallback(Checkpoint checkpoint, LatLng userPosition, MapsView mapsView, IMapsPresenter mapsPresenter) {
        this.initialCheckpoint = checkpoint;
        this.mapsView = mapsView;
        this.userPosition = userPosition;
        this.mapsPresenter = mapsPresenter;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mapsPresenter.setMap(mMap);
        uiSettings = mMap.getUiSettings();

        uiSettings.setMapToolbarEnabled(false);
        uiSettings.setMyLocationButtonEnabled(true);
        uiSettings.setCompassEnabled(true);

        mapsView.setLocationEnabled(mMap);

        LatLng initialCheckpointLoc = new LatLng(initialCheckpoint.coordinates.get(1), initialCheckpoint.coordinates.get(0));
        mMap.addMarker(new MarkerOptions()
                .position(initialCheckpointLoc)
                .title(initialCheckpoint.name))
                .setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE));

        LatLngBounds bounds = new LatLngBounds.Builder()
                .include(userPosition)
                .include(initialCheckpointLoc)
                .build();
        mMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, 220));
    }
}
