package pickapp.landmarker.mapas;

import android.location.Location;

import com.google.android.gms.maps.GoogleMap;

import pickapp.landmarker.bean.Checkpoint;

public interface IMapsPresenter {
    void setMap(GoogleMap googleMap);
    void setNextMarker(Checkpoint checkpoint, Location location);
}
