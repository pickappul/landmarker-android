package pickapp.landmarker.mapas;

import android.location.Location;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;

import pickapp.landmarker.bean.Checkpoint;


class MapsPresenter implements IMapsPresenter {
    private GoogleMap googleMap;

    @Override
    public void setMap(GoogleMap googleMap) {
        this.googleMap = googleMap;
    }

    @Override
    public void setNextMarker(Checkpoint checkpoint, Location location) {
        LatLng latLng = new LatLng(
                checkpoint.coordinates.get(1),
                checkpoint.coordinates.get(0)
        );

        LatLng currentLocation = new LatLng(
                location.getLatitude(),
                location.getLongitude()
        );

        googleMap.clear();
        googleMap.addMarker(new MarkerOptions()
                .position(latLng)
                .title(checkpoint.name))
                .setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE));

        LatLngBounds bounds = new LatLngBounds.Builder()
                .include(currentLocation)
                .include(latLng)
                .build();
        googleMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, 220));
    }

}
