package pickapp.landmarker.mapas;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import pickapp.landmarker.R;
import pickapp.landmarker.bean.Checkpoint;
import pickapp.landmarker.challenge.ChallengeFragment;
import pickapp.landmarker.checkpoints.CheckpointsPresenter;
import pickapp.landmarker.checkpoints.ICheckpointsPresenter;
import pickapp.landmarker.location.LMLocationListener;
import pickapp.landmarker.rutas.TravelsFragment;

public class MapsActivity extends AppCompatActivity implements MapsView, ChallengeFragment.ChallengeListener {
    private static final int REQUEST_LOCATION_CODE = 0;

    @BindView(R.id.main_coordinator) CoordinatorLayout coordinatorLayout;
    @BindView(R.id.toolbar) Toolbar toolbar;

    SupportMapFragment mapFragment;
    LMLocationListener locationListener;
    LocationManager locationManager;
    TravelsFragment travelsFragment;
    Location lastKnownLocation;
    ICheckpointsPresenter checkpointsPresenter;
    List<Checkpoint> checkpoints;
    IMapsPresenter mapsPresenter;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        ButterKnife.bind(this);

        // Adding Toolbar to Main screen
        setSupportActionBar(toolbar);
        setTitle("Travels");

        checkpointsPresenter = new CheckpointsPresenter(this);
        locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            this.requestLocationPermissions();
        } else {
            if (findViewById(R.id.fragment_container) != null) {
                if (savedInstanceState != null) return;
                showListOfTravels();
            }
        }

    }

    @Override
    protected void onStop() {
        super.onStop();
        this.removeGPSUpdates();
    }

    @Override
    public void requestLocationPermissions(){
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
            Snackbar.make(coordinatorLayout, R.string.permission_location_rationale, Snackbar.LENGTH_INDEFINITE)
                    .setAction(R.string.ok, new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ActivityCompat.requestPermissions(MapsActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION_CODE);
                }
            }).show();
        } else {
            ActivityCompat.requestPermissions(MapsActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_LOCATION_CODE: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    showListOfTravels();
                } else {
                    this.finish();
                }
            }
        }
    }

    @Override
    public void showTravelMap(Checkpoint checkpoint) {
        startLMLocationListener();

        mapFragment = SupportMapFragment.newInstance();
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_container, mapFragment)
                .addToBackStack(null)
                .commit();

        LatLng userPosition = new LatLng(lastKnownLocation.getLatitude(), lastKnownLocation.getLongitude());
        this.mapsPresenter = new MapsPresenter();
        mapFragment.getMapAsync(new MapReadyCallback(checkpoint, userPosition, this, mapsPresenter));
    }

    @Override
    public void showListOfTravels() {
        setLastKnownLocation();

        travelsFragment = new TravelsFragment();
        Bundle bundle = new Bundle();
        bundle.putDouble(TravelsFragment.LATITUDE, lastKnownLocation.getLatitude());
        bundle.putDouble(TravelsFragment.LONGITUDE, lastKnownLocation.getLongitude());
        travelsFragment.setArguments(bundle);

        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.fragment_container, travelsFragment)
                .commit();
    }

    @Override
    public void startTravel(String travelId) {
        checkpointsPresenter.getCheckpoints(travelId);
    }

    @Override
    public void startLMLocationListener() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            this.requestLocationPermissions();
        }else {
            Location initialCheckpointLocation = new Location(LocationManager.GPS_PROVIDER);
            initialCheckpointLocation.setLatitude(checkpoints.get(0).coordinates.get(1));
            initialCheckpointLocation.setLongitude(checkpoints.get(0).coordinates.get(0));
            locationListener = new LMLocationListener(this, initialCheckpointLocation);
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 2000, 5.0f, locationListener);
        }
    }

    @Override
    public void setLocationEnabled(GoogleMap googleMap) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            this.requestLocationPermissions();
        }else {
            googleMap.setMyLocationEnabled(true);
        }
    }

    @Override
    public void removeGPSUpdates() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            this.requestLocationPermissions();
        }else{
            locationManager.removeUpdates(locationListener);
        }
    }

    @Override
    public void setToolbarTitle(String title) {
        this.setTitle(title);
    }

    private void setLastKnownLocation() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            this.requestLocationPermissions();
        }else{
            lastKnownLocation = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            if (lastKnownLocation == null){
                lastKnownLocation = new Location(LocationManager.NETWORK_PROVIDER);
                lastKnownLocation.setLatitude(-12.084048);
                lastKnownLocation.setLongitude(-76.971265);
            }
        }
    }

    @Override
    public void setCheckpoints(List<Checkpoint> checkpoints) {
        this.checkpoints = checkpoints;
    }

    @Override
    public void showChallengeDialog() {
        Log.i("Challenge", "Showing challenge dialog");
        Checkpoint checkpoint = checkpoints.get(0);
        if (checkpoint != null){
            Bundle bundle = new Bundle();
            bundle.putSerializable("checkpoint", checkpoint);

            ChallengeFragment challengeFragment = new ChallengeFragment();
            challengeFragment.setArguments(bundle);
            challengeFragment.show(getSupportFragmentManager(), "challenge_fragment");
        }
    }

    @Override
    public void showError(String error) {
        Snackbar.make(coordinatorLayout, error, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void onChallengeWon() {
        Checkpoint checkpoint = checkpoints.get(0);
        checkpoints.remove(checkpoint);

        if (!checkpoints.isEmpty()) {
            checkpoint = checkpoints.get(0);

            Location location = new Location(LocationManager.GPS_PROVIDER);
            location.setLatitude(checkpoint.coordinates.get(1));
            location.setLongitude(checkpoint.coordinates.get(0));
            locationListener.setNextCheckpointLocation(location);

            mapsPresenter.setNextMarker(checkpoint, locationListener.location);
            Snackbar.make(coordinatorLayout, "You answered correctly.", Snackbar.LENGTH_LONG).show();
        }
        else{
            Snackbar.make(coordinatorLayout, "Ganó la ruta", Snackbar.LENGTH_LONG).show();
        }
    }

    @Override
    public void onChallengeFailed() {
        Snackbar.make(coordinatorLayout, "You answered incorrectly.", Snackbar.LENGTH_LONG).show();
    }
}
