package pickapp.landmarker.challenge;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import pickapp.landmarker.bean.Checkpoint;

public final class ChallengePresenterImpl implements ChallengePresenter {
    private View mView;

    public ChallengePresenterImpl(View view) {
        this.mView = view;
    }

    @Override
    public void setUpChallenge(Checkpoint checkpoint) {
        List<String> options = new ArrayList<>(checkpoint.options);
        int correctIndex = (new Random()).nextInt(options.size() + 1);
        options.add(correctIndex, checkpoint.answer);
        for (int i = 0; i < options.size(); ++i) {
            if (i == correctIndex)
                mView.displayOption(options.get(i), CORRECT_TAG);
            else
                mView.displayOption(options.get(i), INCORRECT_TAG);
        }
    }

    @Override
    public void onChallengeAnswer(int tag) {
        if (tag == CORRECT_TAG)
            mView.onChallengeWon();
        else
            mView.onChallengeFailed();
    }
}
