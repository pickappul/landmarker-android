package pickapp.landmarker.challenge;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import pickapp.landmarker.R;
import pickapp.landmarker.bean.Checkpoint;

public final class ChallengeFragment extends DialogFragment
        implements android.view.View.OnClickListener, ChallengePresenter.View {

    @BindView(R.id.btnChallengeSubmit) Button mChallengeSubmit;
    @BindView(R.id.tviChallengeQuestion) TextView mChallengeQuestion;
    @BindView(R.id.rgChallengeOptions) RadioGroup mChallengeOptions;

    private ChallengeListener mListener;
    private ChallengePresenter mPresenter;
    private LayoutInflater mInflater;
    private ViewGroup mContainer;

    public ChallengeFragment() {
    }

    @Override
    public android.view.View onCreateView(LayoutInflater inflater, ViewGroup container,
                                          Bundle savedInstanceState) {
        mInflater = inflater;
        mContainer = container;
        android.view.View view = mInflater.inflate(R.layout.fragment_challenge, mContainer, false);
        ButterKnife.bind(this, view);
        setPresenter(new ChallengePresenterImpl(this));

        Bundle arguments = getArguments();
        Checkpoint checkpoint = (Checkpoint)arguments.getSerializable("checkpoint");

        mChallengeSubmit.setOnClickListener(this);
        mChallengeQuestion.setText(checkpoint.question);
        mPresenter.setUpChallenge(checkpoint);
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mListener = (ChallengeListener)context;
    }

    @Override
    public void onClick(android.view.View view) {
        int checked = mChallengeOptions.getCheckedRadioButtonId();
        int tag = (Integer)mChallengeOptions.findViewById(checked).getTag();
        mPresenter.onChallengeAnswer(tag);
    }

    @Override
    public void onChallengeWon() {
        mListener.onChallengeWon();
        this.dismiss();
    }

    @Override
    public void onChallengeFailed() {
        mListener.onChallengeFailed();
        //this.dismiss();
    }

    @Override
    public void setPresenter(ChallengePresenter presenter) {
        this.mPresenter = presenter;
    }

    @Override
    public void displayOption(String answer, int tag) {
        RadioButton radioButton = (RadioButton)mInflater.inflate(R.layout.option_challenge, mContainer, false);
        radioButton.setText(answer);
        radioButton.setId(android.view.View.generateViewId());
        radioButton.setTag(tag);
        mChallengeOptions.addView(radioButton);
    }

    public interface ChallengeListener {
        void onChallengeWon();
        void onChallengeFailed();
    }
}
