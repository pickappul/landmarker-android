package pickapp.landmarker.challenge;

import pickapp.landmarker.bean.Checkpoint;

public interface ChallengePresenter {
    int CORRECT_TAG = 1;
    int INCORRECT_TAG = 0;

    interface View {
        void setPresenter(ChallengePresenter presenter);
        void displayOption(String answer, int tag);
        void onChallengeWon();
        void onChallengeFailed();
    }

    void setUpChallenge(Checkpoint checkpoint);
    void onChallengeAnswer(int tag);
}
