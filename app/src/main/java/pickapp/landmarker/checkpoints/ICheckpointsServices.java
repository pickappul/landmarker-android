package pickapp.landmarker.checkpoints;

import java.util.List;

import pickapp.landmarker.bean.Checkpoint;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface ICheckpointsServices {
    @GET("travel/{travelId}/points")
    Call<List<Checkpoint>> getCheckpoints(@Path("travelId") String travelId);
}
