package pickapp.landmarker.checkpoints;

public interface ICheckpointsPresenter {
    void getCheckpoints(String travelId);
}
