package pickapp.landmarker.checkpoints;

import java.util.List;

import pickapp.landmarker.bean.Checkpoint;
import pickapp.landmarker.mapas.MapsView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CheckpointsListCallback implements Callback<List<Checkpoint>> {
    private MapsView mapsView;

    public CheckpointsListCallback(MapsView mapsView) {
        this.mapsView = mapsView;
    }

    @Override
    public void onResponse(Call<List<Checkpoint>> call, Response<List<Checkpoint>> response) {
        List<Checkpoint> checkpoints = response.body();
        if (checkpoints != null && checkpoints.size() != 0){
            mapsView.setCheckpoints(checkpoints);
            mapsView.showTravelMap(checkpoints.get(0));
        }else{
            mapsView.showError("La lista esta vacia");
        }
    }

    @Override
    public void onFailure(Call<List<Checkpoint>> call, Throwable t) {
        mapsView.showError("Error de conexión.");
    }
}
