package pickapp.landmarker.checkpoints;

import pickapp.landmarker.mapas.MapsView;
import pickapp.landmarker.remote.RetroConection;
import pickapp.landmarker.rutas.ITravelListServices;

public class CheckpointsPresenter implements ICheckpointsPresenter {
    MapsView mapsView;

    public CheckpointsPresenter(MapsView mapsView) {
        this.mapsView = mapsView;
    }

    @Override
    public void getCheckpoints(String travelId) {
        ICheckpointsServices services = RetroConection.getRetro().create(ICheckpointsServices.class);
        CheckpointsListCallback callback = new CheckpointsListCallback(mapsView);
        services.getCheckpoints(travelId).enqueue(callback);
    }
}
