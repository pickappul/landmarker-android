package pickapp.landmarker.registro;

import pickapp.landmarker.bean.Usuario;

/**
 * Created by ENRIQUE on 30/10/2016.
 */

public interface RegisterPresenter {

     void registerUser(Usuario user);

}
