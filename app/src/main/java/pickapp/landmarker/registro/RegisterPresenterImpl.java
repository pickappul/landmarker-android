package pickapp.landmarker.registro;

import android.util.Log;

import pickapp.landmarker.bean.ServerResponse;
import pickapp.landmarker.bean.Usuario;
import pickapp.landmarker.remote.RetroConection;
import pickapp.landmarker.remote.Service;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by ENRIQUE on 30/10/2016.
 */

public class RegisterPresenterImpl implements RegisterPresenter {
    private RegisterView mView;

    public RegisterPresenterImpl(RegisterView view){
        mView=view;
    }

    @Override
    public void registerUser(Usuario user) {
        Retrofit retrofit = RetroConection.getRetro();

        Service service= retrofit.create(Service.class);

        service.registerUser(user).enqueue(new Callback<ServerResponse>() {
            @Override
            public void onResponse(Call call, Response response) {
                String msg = ((ServerResponse) response.body()).getMsg();

                if (msg!=null && msg.equalsIgnoreCase("ok")){
                    mView.correctRegister();
                }else{
                    mView.incorrectRegister();
                }
            }

            @Override
            public void onFailure(Call<ServerResponse> call, Throwable t) {
                Log.e("Error de Conexión",t.getMessage());
            }
        });
    }
}
