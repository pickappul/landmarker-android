package pickapp.landmarker.registro;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.graphics.drawable.VectorDrawableCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import pickapp.landmarker.R;
import pickapp.landmarker.bean.Usuario;
import pickapp.landmarker.login.LoginActivity;

public class RegisterActivity extends AppCompatActivity implements RegisterView {

    @BindView(R.id.eteUserName) EditText eteUserName;
    @BindView(R.id.eteName) EditText eteName;
    @BindView(R.id.eteLastName) EditText eteLastName;
    @BindView(R.id.etePassword) EditText etePassword;
    @BindView(R.id.eteConfirmPassword) EditText eteConfirmPassword;
    @BindView(R.id.eteMail) EditText eteMail;

    @BindView(R.id.layoutUserName) TextInputLayout layoutUserName;
    @BindView(R.id.layoutName) TextInputLayout layoutName;
    @BindView(R.id.layoutLastName) TextInputLayout layoutLastName;
    @BindView(R.id.layoutPassword) TextInputLayout layoutPassword;
    @BindView(R.id.layoutConfirmPassword) TextInputLayout layoutConfirmPassword;
    @BindView(R.id.layoutMail) TextInputLayout layoutMail;

    private RegisterPresenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        Toolbar toolbar = (Toolbar) findViewById(R.id.registerToolbar);
        setSupportActionBar(toolbar);

        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null) {
            VectorDrawableCompat indicator = VectorDrawableCompat.create(getResources(), R.drawable.ic_arrow_back_black_24dp, getTheme());
            indicator.setTint(ResourcesCompat.getColor(getResources(), R.color.white, getTheme()));
            supportActionBar.setHomeAsUpIndicator(indicator);
            supportActionBar.setDisplayHomeAsUpEnabled(true);
        }

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        ButterKnife.bind(this);
        setPresenter(new RegisterPresenterImpl(this));
    }

    @Override
    public void setPresenter(RegisterPresenter presenter) {
        this.mPresenter = presenter;
    }

    @Override
    public void correctRegister() {
        Toast.makeText(this, "Registro Correcto", Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(this, LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        //finish();
    }

    @Override
    public void incorrectRegister() {
        Toast.makeText(this, "Registro InCorrecto", Toast.LENGTH_SHORT).show();
    }

    public void onRegisterClick(View view) {

        eteUserName.addTextChangedListener(new RegisterValidation(eteUserName, layoutUserName));
        eteName.addTextChangedListener(new RegisterValidation(eteName, layoutName));
        eteLastName.addTextChangedListener(new RegisterValidation(eteLastName, layoutLastName));
        etePassword.addTextChangedListener(new RegisterValidation(etePassword, layoutPassword));
        eteConfirmPassword.addTextChangedListener(new RegisterValidation(eteConfirmPassword, layoutConfirmPassword));
        eteMail.addTextChangedListener(new RegisterValidation(eteMail, layoutMail));

        String username = eteUserName.getText().toString();
        String name = eteName.getText().toString();
        String lastName = eteLastName.getText().toString();
        String password = etePassword.getText().toString();
        String confirmPassword = eteConfirmPassword.getText().toString();
        String mail = eteMail.getText().toString();

        boolean incorrectValid = false;

        if (TextUtils.isEmpty(username)) {
            incorrectValid = true;
            layoutUserName.setError(getString(R.string.field_required));
        }
        if (TextUtils.isEmpty(name)) {
            incorrectValid = true;
            layoutName.setError(getString(R.string.field_required));
        }
        if (TextUtils.isEmpty(lastName)) {
            incorrectValid = true;
            layoutLastName.setError(getString(R.string.field_required));
        }
        if (TextUtils.isEmpty(password)) {
            incorrectValid = true;
            layoutPassword.setError(getString(R.string.field_required));
        }
        if (password.length() < 5) {
            incorrectValid = true;
            layoutPassword.setError(getString(R.string.password_length_error));
        }
        if (TextUtils.isEmpty(confirmPassword)) {
            incorrectValid = true;
            layoutConfirmPassword.setError(getString(R.string.field_required));
        }
        if (!confirmPassword.equals(password)) {
            incorrectValid = true;
            layoutConfirmPassword.setError(getString(R.string.mismatch_error));
        }
        if (TextUtils.isEmpty(mail)) {
            incorrectValid = true;
            layoutMail.setError(getString(R.string.field_required));
        }
        if (incorrectValid) return;

        Usuario usuario = new Usuario(name, lastName, username, mail, password);
        mPresenter.registerUser(usuario);

    }

    //Esta clase se encarga de validar cada ves que se ingresa un valor en el textfield.

    public class RegisterValidation implements TextWatcher {
        private EditText editText;
        private TextInputLayout layout;

        public RegisterValidation(EditText editText, TextInputLayout layout) {
            this.editText = editText;
            this.layout = layout;
        }

        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        @Override
        public void afterTextChanged(Editable editable) {
            if (editText.getId() == etePassword.getId()) {
                boolean v = validateEmpty() && validatePassword();
            } else if (editText.getId() == eteConfirmPassword.getId()) {
                boolean v = validateEmpty() && validateConfirmPassword();
            } else {
                validateEmpty();
            }
        }

        public boolean validateEmpty() {
            if (editText.getText().toString().isEmpty()) {
                layout.setError(getString(R.string.field_required));
                return false;
            } else {
                layout.setError(null);
                return true;
            }
        }

        public boolean validatePassword() {
            if (editText.getText().toString().length() < 6) {
                layout.setError(getString(R.string.password_length_error));
                return false;
            } else {
                layout.setError(null);
                return true;
            }
        }

        public boolean validateConfirmPassword() {
            if (!editText.getText().toString().equals(etePassword.getText().toString())) {
                layout.setError(getString(R.string.mismatch_error));
                return false;
            } else {
                layout.setError(null);
                return true;
            }
        }
    }

}
