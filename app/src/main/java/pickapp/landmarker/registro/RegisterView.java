package pickapp.landmarker.registro;

/**
 * Created by ENRIQUE on 30/10/2016.
 */

public interface RegisterView {

    void correctRegister();
    void incorrectRegister();
    void setPresenter(RegisterPresenter presenter);

}
