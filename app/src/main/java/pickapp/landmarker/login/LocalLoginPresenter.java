package pickapp.landmarker.login;

import android.content.Intent;

import java.net.HttpURLConnection;

import pickapp.landmarker.bean.LoginResponse;
import pickapp.landmarker.bean.Usuario;
import pickapp.landmarker.remote.RetroConection;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


final class LocalLoginPresenter implements LoginPresenter {
    private LoginPresenter.View mView;
    private LocalLoginService mService;

    LocalLoginPresenter(LoginPresenter.View view) {
        this.mView = view;
        mService = RetroConection.getRetro().create(LocalLoginService.class);
    }

    LocalLoginPresenter(LoginPresenter.View view, LocalLoginService service) {
        this.mView = view;
        this.mService = service;
    }

    @Override
    public void doLogin() {
        Usuario usuario = mView.getUser();
        mService.login(usuario).enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                if (response.isSuccessful()) {
                    mView.finishLogin();
                } else if (response.code() == HttpURLConnection.HTTP_UNAUTHORIZED) {
                    mView.showError("Usuario y/o contraseña incorrecta.");
                } else {
                    mView.showError("Error de login.");
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                mView.showError("Error de conexión.");
            }
        });
    }

    @Override
    public void handleLoginResult(int requestCode, int resultCode, Intent data) {
    }
}
