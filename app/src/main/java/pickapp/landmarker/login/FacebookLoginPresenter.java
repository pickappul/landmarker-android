package pickapp.landmarker.login;

import android.content.Intent;
import android.util.Log;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;

final class FacebookLoginPresenter implements LoginPresenter {
    private LoginPresenter.View mView;
    private CallbackManager mCallbackManager;

    FacebookLoginPresenter(LoginPresenter.View view) {
        this.mView = view;
        buildPresenter();
    }

    private void buildPresenter() {
        mCallbackManager = CallbackManager.Factory.create(); //crear cb factory

        LoginManager.getInstance().registerCallback(mCallbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.d("Success", "FacebookLogin");
                mView.finishLogin();
            }

            @Override
            public void onCancel() {
                mView.showError("Se cancelo login");
            }

            @Override
            public void onError(FacebookException error) {
                mView.showError("Error en el login");
            }
        });
    }

    @Override
    public void doLogin() {
        mView.startLoginActivity(Type.FACEBOOK, null);
    }

    @Override
    public void handleLoginResult(int requestCode, int resultCode, Intent data) {
        mCallbackManager.onActivityResult(requestCode, resultCode, data);
    }
}
