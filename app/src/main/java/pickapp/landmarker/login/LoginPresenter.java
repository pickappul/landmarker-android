package pickapp.landmarker.login;

import android.content.Intent;

import pickapp.landmarker.bean.Usuario;

public interface LoginPresenter {
    enum Type {
        LOCAL,
        GOOGLE,
        FACEBOOK
    }

    interface View {
        Usuario getUser();
        void finishLogin();
        void showError(String message);
        void startLoginActivity(LoginPresenter.Type type, Intent intent);
    }

    class Factory {
        static LoginPresenter make(Type type, LoginPresenter.View view) {
            switch (type) {
                case LOCAL:
                    return new LocalLoginPresenter(view);
                case GOOGLE:
                    return new GoogleLoginPresenter(view);
                case FACEBOOK:
                    return new FacebookLoginPresenter(view);
                default:
                    return null;
            }
        }
    }

    void doLogin();
    void handleLoginResult(int requestCode, int resultCode, Intent data);
}
