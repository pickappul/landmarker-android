package pickapp.landmarker.login;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;

import java.util.Arrays;

import butterknife.BindView;
import butterknife.ButterKnife;
import pickapp.landmarker.R;
import pickapp.landmarker.bean.Usuario;
import pickapp.landmarker.mapas.MapsActivity;
import pickapp.landmarker.registro.RegisterActivity;

public final class LoginActivity extends AppCompatActivity implements LoginPresenter.View, View.OnClickListener {
    private static final int RC_GOOGLE_SIGN_IN = 1200;
    private static final int RC_FACEBOOK = 1300;

    @BindView(R.id.eteUser) EditText eteUser;
    @BindView(R.id.etePassword) EditText etePassword;
    @BindView(R.id.tviRegistro) TextView tviRegistro;
    @BindView(R.id.butLocalLogin) Button butLocalLogin;
    @BindView(R.id.butGoogleLogin) Button butGoogleLogin;
    @BindView(R.id.butFacebookLogin) Button butFacebookLogin;

    private LoginPresenter mLocalPresenter;
    private LoginPresenter mGooglePresenter;
    private LoginPresenter mFacebookPresenter;

    /*
     * Activity Lifecycle methods
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //FacebookSdk.sdkInitialize(getApplicationContext(), RC_FACEBOOK);

        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        mLocalPresenter = LoginPresenter.Factory.make(LoginPresenter.Type.LOCAL, this);
        mGooglePresenter = LoginPresenter.Factory.make(LoginPresenter.Type.GOOGLE, this);
        mFacebookPresenter = LoginPresenter.Factory.make(LoginPresenter.Type.FACEBOOK, this);

        tviRegistro.setOnClickListener(this);
        butLocalLogin.setOnClickListener(this);
        butGoogleLogin.setOnClickListener(this);
        butFacebookLogin.setOnClickListener(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    /*
     * Handle Activity Results
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_GOOGLE_SIGN_IN) {
            mGooglePresenter.handleLoginResult(requestCode, resultCode, data);
        } else if (FacebookSdk.isFacebookRequestCode(requestCode)) {
            mFacebookPresenter.handleLoginResult(requestCode, resultCode, data);
        }
    }

    /*
     * OnClickListeners
     */
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tviRegistro: {
                Intent intent = new Intent(this, RegisterActivity.class);
                startActivity(intent);
                return;
            }
            case R.id.butLocalLogin:
                mLocalPresenter.doLogin();
                return;
            case R.id.butGoogleLogin:
                mGooglePresenter.doLogin();
                return;
            case R.id.butFacebookLogin:
                mFacebookPresenter.doLogin();
                return;
        }
    }

    /*
     * View methods
     */
    @Override
    public Usuario getUser() {
        return new Usuario(eteUser.getText().toString(), etePassword.getText().toString());
    }

    @Override
    public void finishLogin() {
        Intent intent = new Intent(this, MapsActivity.class);
        startActivity(intent);
    }

    @Override
    public void showError(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void startLoginActivity(LoginPresenter.Type type, Intent intent) {
        switch (type) {
            case GOOGLE:
                startActivityForResult(intent, RC_GOOGLE_SIGN_IN);
                return;
            case FACEBOOK:
                LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile", "user_friends"));
                return;
        }
    }
}