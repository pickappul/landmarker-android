package pickapp.landmarker.login;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;

// https://developers.google.com/identity/sign-in/android/start-integrating
// https://developers.google.com/identity/sign-in/android/sign-in
final class GoogleLoginPresenter implements LoginPresenter {
    private LoginPresenter.View mView;
    private GoogleApiClient mGoogleApiClient;

    GoogleLoginPresenter(LoginPresenter.View view) {
        mView = view;
        buildPresenter();
    }

    private void buildPresenter() {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken("933146405573-tr46eolufk08t5h5vt4hiad6p095j19h.apps.googleusercontent.com")
                .requestEmail()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder((Context)mView)
                .enableAutoManage((AppCompatActivity)mView, new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
                        // Do something about connection failure
                    }
                })
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
    }

    @Override
    public void doLogin() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        mView.startLoginActivity(Type.GOOGLE, signInIntent);
    }

    @Override
    public void handleLoginResult(int requestCode, int resultCode, Intent data) {
        GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);

        if (result.isSuccess()) {
            mView.finishLogin();
        } else {
            Log.d("Google result", result.getStatus().toString());
            mView.showError("Error de login");
        }
    }
}
