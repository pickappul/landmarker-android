package pickapp.landmarker.login;

import pickapp.landmarker.bean.LoginResponse;
import pickapp.landmarker.bean.ServerResponse;
import pickapp.landmarker.bean.Usuario;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

interface LocalLoginService {
    @Headers({
        "Accept: application/json",
        "Content-Type: application/json"
    })
    @POST("login")
    Call<LoginResponse> login(@Body Usuario usuario);
}
