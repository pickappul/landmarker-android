package pickapp.landmarker.location;

import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.util.Log;

import pickapp.landmarker.mapas.MapsView;

public class LMLocationListener implements LocationListener {
    private final String TAG = "LocationListener";
    private Location checkpointLocation;
    private boolean checkpointEntered;
    private MapsView mapsView;
    public Location location;

    public LMLocationListener(MapsView mapsView, Location initialCheckpointLocation) {
        this.mapsView = mapsView;
        this.checkpointLocation = initialCheckpointLocation;
        this.checkpointEntered = false;
    }

    @Override
    public void onLocationChanged(Location location) {
        Log.i(TAG, "Location changed!");
        this.location = location;

        if (checkpointLocation != null && !checkpointEntered){
            double distance = Haversine.distance(location, checkpointLocation);
            if (distance < 20d){
                Log.i(TAG, "Entered checkpoint area");
                mapsView.showChallengeDialog();
                checkpointEntered = true;
            }
        }
    }

    public void setNextCheckpointLocation(Location checkpointLocation){
        this.checkpointLocation = checkpointLocation;
        this.checkpointEntered = false;
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {
        Log.i(TAG, "Status changed. Status " + i);
    }

    @Override
    public void onProviderEnabled(String s) {
        Log.i(TAG, "Provider enabled. " + s);
    }

    @Override
    public void onProviderDisabled(String s) {
        Log.i(TAG, "Provider disabled. " + s);
    }
}
