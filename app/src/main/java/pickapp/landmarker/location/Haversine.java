package pickapp.landmarker.location;

import android.location.Location;

/**
 * Jason Winn
 * http://jasonwinn.org
 * Created July 10, 2013
 *
 * Description: Small class that provides approximate distance between
 * two points using the Haversine formula.
 *
 * Call in a static context:
 * Haversine.distance(47.6788206, -122.3271205,
 *                    47.6788206, -122.5271205)
 * --> 14.973190481586224 [km]
 *
 */

class Haversine {
    private static final int EARTH_RADIUS = 6371; // Approx Earth radius in KM

    static double distance(Location startLocation, Location endLocation) {

        double dLat  = Math.toRadians((endLocation.getLatitude() - startLocation.getLatitude()));
        double dLong = Math.toRadians((endLocation.getLongitude() - startLocation.getLongitude()));

        double startLat = Math.toRadians(startLocation.getLatitude());
        double endLat   = Math.toRadians(endLocation.getLatitude());

        double a = haversin(dLat) + Math.cos(startLat) * Math.cos(endLat) * haversin(dLong);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

        return EARTH_RADIUS * c * 1000d; // <-- d
    }

    private static double haversin(double val) {
        return Math.pow(Math.sin(val / 2), 2);
    }
}