package pickapp.landmarker.bean;

/**
 * Created by ENRIQUE on 01/11/2016.
 */

public class ServerResponse {
    private String msg;

    public ServerResponse(String msg) {
        this.msg = msg;
    }

    public ServerResponse() {
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
