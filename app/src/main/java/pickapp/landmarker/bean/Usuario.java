package pickapp.landmarker.bean;

import java.util.Date;

public class Usuario {
    public String name;
    public String lastName;
    public String username;
    public String email;
    public Date BirthDate;
    public String sex;
    public String password;


    public Usuario() {
    }

    public Usuario(String name, String lastName, String username, String email, String password) {
        this.name = name;
        this.lastName = lastName;
        this.username = username;
        this.email = email;
        this.password = password;
    }

    public Usuario(String username, String password) {
        this.password = password;
        this.username = username;
    }
}
