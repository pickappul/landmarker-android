package pickapp.landmarker.bean;

public class LoginResponse {
    public String status;

    public LoginResponse(String status) {
        this.status = status;
    }

    public LoginResponse() {
    }
}
