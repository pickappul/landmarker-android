package pickapp.landmarker.bean;

import java.io.Serializable;
import java.util.List;

public class Checkpoint implements Serializable {
    public String name;
    //coordinates[0] is longitude and coordinates[1] is latitude
    public List<Float> coordinates;
    public String question;
    public String answer;
    public List<String> options;

    public Checkpoint(String name, List<Float> coordinates, String question, String answer, List<String> options) {
        this.name = name;
        this.coordinates = coordinates;
        this.question = question;
        this.answer = answer;
        this.options = options;
    }

    public Checkpoint() {
    }
}
