package pickapp.landmarker.bean;

import java.util.List;

public class Travel {
    public String id;
    public String name;
    //coordinates[0] is longitude and coordinates[1] is latitude
    public List<Float> coordinates;
    public String image;
    public String description;
    public Float distance;

    public Travel(String id, String name, List<Float> coordinates, Float distance) {
        this.id = id;
        this.name = name;
        this.coordinates = coordinates;
        this.distance = distance;
    }

    public Travel() {
    }
}
