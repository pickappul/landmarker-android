package pickapp.landmarker.remote;

import java.util.List;

import pickapp.landmarker.bean.ServerResponse;
import pickapp.landmarker.bean.Usuario;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface Service {

    @POST("register")
    Call<ServerResponse> registerUser(@Body Usuario usuario);

    @GET("usuario")
    Call<List<Usuario>> getUsuarios();

    @POST("login")
    Call<ServerResponse> login(@Body Usuario usuario);


}
