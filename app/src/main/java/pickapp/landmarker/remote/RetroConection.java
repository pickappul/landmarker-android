package pickapp.landmarker.remote;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetroConection {

    private static final String url_base="https://landmarker.meido.ninja/";

    public static Retrofit getRetro(){
        return new Retrofit.Builder()
                .baseUrl(url_base)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }
}