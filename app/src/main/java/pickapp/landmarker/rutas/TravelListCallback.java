package pickapp.landmarker.rutas;

import java.util.List;

import pickapp.landmarker.bean.Travel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TravelListCallback implements Callback<List<Travel>> {
    TravelsView view;

    public TravelListCallback(TravelsView view) {
        this.view = view;
    }

    @Override
    public void onResponse(Call<List<Travel>> call, Response<List<Travel>> response) {
        if (response.body() != null){
            view.showTravels(response.body());
        }
        else{
            view.showError("No existen rutas");
        }
    }

    @Override
    public void onFailure(Call<List<Travel>> call, Throwable t) {
        view.showError("Conexión falló.");
    }
}
