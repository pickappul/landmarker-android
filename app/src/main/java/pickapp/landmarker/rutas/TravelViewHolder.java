package pickapp.landmarker.rutas;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import pickapp.landmarker.R;
import pickapp.landmarker.bean.Travel;
import pickapp.landmarker.mapas.MapsView;

public class TravelViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
    Travel travel;
    Button butStart;
    ImageView tviMain;
    TextView tviName;
    TextView tviDescription;
    TextView tviDistance;
    Context context;

    TravelViewHolder(LayoutInflater inflater, ViewGroup parent, Context context) {
        super(inflater.inflate(R.layout.travel_item, parent, false));
        this.context = context;

        tviMain = (ImageView) itemView.findViewById(R.id.travel_image);
        tviName = (TextView) itemView.findViewById(R.id.travel_name);
        tviDescription = (TextView) itemView.findViewById(R.id.travel_description);
        tviDistance = (TextView) itemView.findViewById(R.id.travel_distance);

        butStart = (Button) itemView.findViewById(R.id.travel_start);
        butStart.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        MapsView mapsView = (MapsView)this.context;
        mapsView.startTravel(this.travel.id);
        mapsView.setToolbarTitle(this.travel.name);
    }
}
