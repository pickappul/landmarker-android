package pickapp.landmarker.rutas;

import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import pickapp.landmarker.R;
import pickapp.landmarker.bean.Travel;

public class TravelsFragment extends Fragment implements TravelsView{
    public static final String LATITUDE = "latitude";
    public static final String LONGITUDE = "longitude";

    RecyclerView recyclerView;
    Context context;
    ITravelPresenter presenter;
    double latitude;
    double longitude;

    @Override
    public void setArguments(Bundle args) {
        super.setArguments(args);
        latitude = args.getDouble(LATITUDE);
        longitude = args.getDouble(LONGITUDE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        this.recyclerView = (RecyclerView)inflater.inflate(R.layout.recycler_view, container, false);
        this.context = recyclerView.getContext();
        recyclerView.setAdapter(new TravelsContentAdapter());
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        presenter = new TravelPresenter(this);

        return recyclerView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        this.presenter.getTravels(10000, latitude, longitude);
    }

    @Override
    public void showTravels(List<Travel> travels) {
        TravelsContentAdapter contentAdapter = new TravelsContentAdapter(travels, getActivity());
        recyclerView.setAdapter(contentAdapter);
    }

    @Override
    public void showError(String error) {
        Snackbar.make(this.getView(), "Welcome to AndroidHive", Snackbar.LENGTH_LONG);
    }
}
