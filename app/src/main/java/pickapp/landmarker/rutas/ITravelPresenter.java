package pickapp.landmarker.rutas;

public interface ITravelPresenter {
    void getTravels(float distance, double longitude, double latitude);
}
