package pickapp.landmarker.rutas;

import pickapp.landmarker.remote.RetroConection;

public class TravelPresenter implements ITravelPresenter{
    private TravelsView view;

    public TravelPresenter(TravelsView view) {
        this.view = view;
    }

    @Override
    public void getTravels(float distance, double longitude, double latitude) {
        ITravelListServices service = RetroConection.getRetro().create(ITravelListServices.class);

        TravelListCallback callback = new TravelListCallback(view);
        service.getNearestTravels(distance, longitude, latitude).enqueue(callback);
    }
}
