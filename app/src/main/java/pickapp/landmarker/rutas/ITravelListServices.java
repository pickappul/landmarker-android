package pickapp.landmarker.rutas;

import java.util.List;

import pickapp.landmarker.bean.Travel;
import retrofit2.Call;
import retrofit2.http.*;

public interface ITravelListServices {
    @GET("travel/near/{distance}/{longitude}/{latitude}")
    Call<List<Travel>> getNearestTravels(@Path("distance") float distance, @Path("longitude") double longitude, @Path("latitude") double latitude);
}
