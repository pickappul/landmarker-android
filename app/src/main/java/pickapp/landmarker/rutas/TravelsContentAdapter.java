package pickapp.landmarker.rutas;

import android.content.Context;
import android.provider.MediaStore;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.RequestCreator;

import java.util.ArrayList;
import java.util.List;

import pickapp.landmarker.R;
import pickapp.landmarker.bean.Travel;
import pickapp.landmarker.bean.Usuario;

public class TravelsContentAdapter extends RecyclerView.Adapter<TravelViewHolder>{
    private List<Travel> travels;
    private Context context;

    public TravelsContentAdapter(List<Travel> travels, Context context) {
        this.travels = travels;
        this.context = context;
    }

    public TravelsContentAdapter() {
        this.travels = new ArrayList<>();
    }

    @Override
    public TravelViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new TravelViewHolder(LayoutInflater.from(parent.getContext()), parent, context);
    }

    @Override
    public void onBindViewHolder(TravelViewHolder holder, int position) {
        Travel travel = travels.get(position);

        holder.travel = travel;
        holder.tviName.setText(travel.name);

        if (travel.description != null) holder.tviDescription.setText(travel.description);
        if (travel.distance != null) holder.tviDistance.setText(String.format("Distancia %.2f km", travel.distance/1000.0f));

        Picasso picasso = Picasso.with(context);
        RequestCreator requestCreator;
        if (travel.image != null && !travel.image.equals("")) {
            requestCreator = picasso.load(travel.image);
        } else{
            requestCreator = picasso.load("http://menthings.de/wp-content/uploads/2015/02/imagen-lima.jpg");
        }
        requestCreator
                .resize(500, 500)
                .centerCrop()
                .error(R.drawable.ic_report_black_24dp)
                .into(holder.tviMain);
    }

    @Override
    public int getItemCount() {
        return travels.size();
    }
}
