package pickapp.landmarker.rutas;

import android.location.Location;

import java.util.List;

import pickapp.landmarker.bean.Travel;

public interface TravelsView {
    void showTravels(List<Travel> travels);
    void showError(String error);
}
