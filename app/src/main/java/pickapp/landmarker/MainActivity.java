package pickapp.landmarker;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;

import com.facebook.AccessToken;

import pickapp.landmarker.login.LoginActivity;
import pickapp.landmarker.login.LoginPresenter;
import pickapp.landmarker.registro.RegisterView;

public class MainActivity extends AppCompatActivity {

    LoginPresenter presenter;
    ImageView Img;

    RegisterView view;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_main);
        if(AccessToken.getCurrentAccessToken() == null){
            //goLoginScreen();
            setContentView(R.layout.activity_login);
            //presenter.realizarLogin();
        }else{
            setContentView(R.layout.activity_main);
        }
    }

    public void goLoginScreen(){
        Intent intent = new Intent(this, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    /*public void logout(View view){
        LoginManager.getInstance().logOut();
        goLoginScreen();
    }*/

    //Ir al layout de registro
    /*public void onRegistroClick(View view) {
        Intent intent = new Intent(this,RegistroActivity.class);
        startActivity(intent);

    }*/

}
