package pickapp.landmarker.challenge;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;

import pickapp.landmarker.bean.Checkpoint;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class ChallengePresenterTest {

    @Mock ChallengePresenter.View view;
    @InjectMocks ChallengePresenterImpl tested;

    // Mock checkpoint to use
    private Checkpoint checkpoint = new Checkpoint("Name", null, "Question?", "Answer", Arrays.asList("Wrong", "Wrong", "Wrong"));

    @Test
    public void setUpChallenge() throws Exception {
        tested.setUpChallenge(checkpoint);
        verify(view, times(3)).displayOption("Wrong", ChallengePresenter.INCORRECT_TAG);
        verify(view, times(1)).displayOption("Answer", ChallengePresenter.CORRECT_TAG);
    }

    @Test
    public void onChallengeAnswer_Correct() throws Exception {
        tested.onChallengeAnswer(ChallengePresenter.CORRECT_TAG);
        verify(view, times(1)).onChallengeWon();
    }

    @Test
    public void onChallengeAnswer_Incorrect() throws Exception {
        tested.onChallengeAnswer(ChallengePresenter.INCORRECT_TAG);
        verify(view, times(1)).onChallengeFailed();
    }
}